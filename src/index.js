// Importing necessary modules
import fs from 'node:fs';
import path from 'node:path';

/**
 * Reads the 'Local State' file of the Chrome browser and extracts the names and directories of user profiles.
 * 
 * @param {Object} args - The arguments object.
 * @param {string} [args.chromeDataPath] - Optional. The path to Chrome's user data directory. If not provided, defaults to the standard path based on the current operating system.
 * @returns {Promise<Object[]>} - A promise that resolves to an array of objects, each containing the profile name and its directory path.
 */
export default async (args) => {
    try {
        // Determine the default path for Chrome's user data directory based on the operating system
        const defaultPath = process.platform === 'win32'
            ? path.join(process.env.LOCALAPPDATA, 'Google/Chrome/User Data')
            : process.platform === 'darwin'
                ? path.join(process.env.HOME, 'Library/Application Support/Google/Chrome')
                : path.join(process.env.HOME, '.config/google-chrome');
        
        // Use provided path or default to Chrome's standard user data directory
        const chromeDataPath = args.chromeDataPath || defaultPath;

        // Read the 'Local State' file which contains profile information
        const localStatePath = path.join(chromeDataPath, 'Local State');
        const localStateData = fs.readFileSync(localStatePath, 'utf-8');
        const localStateJSON = JSON.parse(localStateData);
        
        // Extract profiles and return an array of objects with both profile name and its directory
        const profiles = Object.keys(localStateJSON.profile.info_cache).map(profileName => {
            return {
                name: profileName,
                dir: path.join(chromeDataPath, profileName === 'Default' ? 'Default' : profileName)
            };
        });

        return profiles;

    } catch (error) {
        // Handle any errors that occur while reading the file or parsing the data
        throw new Error(`Failed to list Chrome user profiles: ${error.message}`);
    }
};