# @fnet/chrome-profiles

## Introduction

The `@fnet/chrome-profiles` tool is designed to help users easily retrieve the names of user profiles from their Google Chrome browser. This can be particularly useful for developers or anyone who needs to manage or access different user profiles on a Chrome installation.

## How It Works

This tool reads the 'Local State' file within the Google Chrome user data directory. It extracts and returns an array of profile names associated with the Chrome browser. By default, it automatically determines the correct file path based on the operating system being used, ensuring a seamless experience.

## Key Features

- **Automatic Path Detection**: The tool automatically locates the Chrome user data directory based on the operating system (Windows, macOS, or Linux).
- **Profile Retrieval**: It efficiently reads the 'Local State' file and extracts the names of all the user profiles available in the Chrome installation.
- **Easy Integration**: With an asynchronous function, it easily integrates into other applications or workflows that require access to Chrome profile information.

## Conclusion

The `@fnet/chrome-profiles` tool is a straightforward utility for extracting user profile names from the Chrome browser, tailored for those who need quick access to this information without delving into Chrome's user data manually.