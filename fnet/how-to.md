# Developer Guide for the `@fnet/chrome-profiles` Library

## Overview

The `@fnet/chrome-profiles` library provides a straightforward way to extract information about user profiles from the Chrome browser. Primarily, it reads the 'Local State' file of the Chrome browser to gather and return a list of user profile names along with their corresponding directory paths. This can be particularly useful for applications needing to interface directly with user-specific Chrome settings or data.

## Installation

To install the `@fnet/chrome-profiles` library, you can use either npm or yarn:

Using npm:
```bash
npm install @fnet/chrome-profiles
```

Using yarn:
```bash
yarn add @fnet/chrome-profiles
```

## Usage

This library is designed to be simple and minimalistic. Here's how you can use it to retrieve Chrome user profiles:

```javascript
import getChromeProfiles from '@fnet/chrome-profiles';

(async () => {
  try {
    const profiles = await getChromeProfiles({ 
      // Optional: You can specify a custom path to Chrome's user data directory.
      // chromeDataPath: '/path/to/custom/chrome/user/data'
    });

    console.log('Chrome User Profiles:', profiles);
  } catch (error) {
    console.error('Error fetching profiles:', error);
  }
})();
```

### Parameters

- **chromeDataPath** (optional): A string to specify the path to Chrome's user data directory. If not provided, the library defaults to the standard user data directories based on the current operating system:
  - Windows: `%LOCALAPPDATA%\Google\Chrome\User Data`
  - macOS: `~/Library/Application Support/Google/Chrome`
  - Linux: `~/.config/google-chrome`

## Examples

Here’s how you might use this library for a typical use case where you want to display all Chrome user profiles:

```javascript
import getChromeProfiles from '@fnet/chrome-profiles';

(async () => {
  try {
    const profiles = await getChromeProfiles();
    profiles.forEach(profile => {
      console.log(`Profile Name: ${profile.name}, Directory: ${profile.dir}`);
    });
  } catch (error) {
    console.error('Error fetching profiles:', error);
  }
})();
```

In this example, the code fetches the profiles and logs each profile’s name and directory to the console.

## Acknowledgement

This library internally utilizes Node.js built-in modules such as `fs` for file system operations and `path` for handling file paths. Acknowledging the efforts of contributors and maintainers of Node.js, which provides these essential modules that enable this library to function effectively.